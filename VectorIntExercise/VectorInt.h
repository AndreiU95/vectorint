#pragma once

class VectorInt
{
public:

  VectorInt();
  ~VectorInt();

  VectorInt(const VectorInt&) = delete;
  VectorInt(VectorInt&&) = delete;
  VectorInt& operator=(const VectorInt&) = delete;
  VectorInt& operator=(VectorInt&&) = delete;

  const int* begin() const;
  const int* end() const;

  const int capacity() const;
  const int size() const;
  int& operator[](int index) const;

  void push_back(int element);
  void pop_back();
  void insert(const int* position, const int* first, const int* last);

private:

  int* _begin;
  int* _end;
  int _capacity;
};