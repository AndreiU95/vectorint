#include <iostream>
#include <assert.h>

#include "VectorInt.h"

int main()
{
  // Initializastion
  {
    VectorInt empty;

    assert(empty.capacity() == 0);
    assert(empty.begin() == nullptr);
    assert(empty.end() == nullptr);
    assert(empty.size() == 0);
  }

  // 'push_back' implementation
  {
    VectorInt vector;
    vector.push_back(10);

    assert(vector.capacity() == 5);
    assert(vector.begin() != nullptr);
    assert(vector.end() != nullptr);
    assert(vector.size() == 1);
    assert(vector[0] == 10);
  }

  // 'push_back' with re-allocation
  {
    VectorInt vector;
    vector.push_back(1);
    vector.push_back(2);
    vector.push_back(3);
    vector.push_back(4);
    vector.push_back(5);
    vector.push_back(6);

    assert(vector.capacity() == 10);
    assert(vector.begin() != nullptr);
    assert(vector.end() != nullptr);
    assert(vector.size() == 6);

    assert(vector[0] == 1);
    assert(vector[1] == 2);
    assert(vector[2] == 3);
    assert(vector[3] == 4);
    assert(vector[4] == 5);
    assert(vector[5] == 6);
  }

  //'pop_back' implementation
  {
    VectorInt vector;
    vector.push_back(1);
    vector.push_back(2);
    vector.push_back(3);
    vector.push_back(4);

    assert(vector.capacity() == 5);
    assert(vector.size() == 4);

    vector.pop_back();

    assert(vector.capacity() == 5);
    assert(vector.size() == 3);
    assert(vector.begin() != nullptr);
    assert(vector.end() != nullptr);
  }

  // popping a vector until empty
  {
    VectorInt vector;
    vector.push_back(1);
    vector.push_back(2);
    vector.push_back(3);

    vector.pop_back();
    vector.pop_back();
    vector.pop_back();

    assert(vector.capacity() == 5);
    assert(vector.begin() == nullptr);
    assert(vector.end() == nullptr);
    assert(vector.size() == 0);
  }

  // pooping a vector after it becomes empty => std::exception expected
  {
    VectorInt vector;
    vector.push_back(1);
    vector.push_back(2);

    vector.pop_back();
    vector.pop_back();

    bool exception = false;
    try
    {
      vector.pop_back();
    }
    catch (std::exception ex)
    {
      exception = true;
    }

    assert(exception);

    assert(vector.capacity() == 5);
    assert(vector.begin() == nullptr);
    assert(vector.end() == nullptr);
    assert(vector.size() == 0);
  }

  // operator[]
  {
    VectorInt vector;
    vector.push_back(1);
    vector.push_back(2);
    vector.push_back(3);
    vector.push_back(4);

    assert(vector[0] == 1);
    assert(vector[1] == 2);
    assert(vector[2] == 3);
    assert(vector[3] == 4);

    vector[0] = 10;
    vector[1] = 11;
    vector[2] = 12;
    vector[3] = 13;

    assert(vector[0] == 10);
    assert(vector[1] == 11);
    assert(vector[2] == 12);
    assert(vector[3] == 13);
  }
  
  // 'operator[]' invalid index
  {
    VectorInt vector;
    vector.push_back(1);
    vector.push_back(2);

    {
      bool exception = false;
      try
      {
        vector[-1] = 0;
      }
      catch (std::exception ex)
      {
        exception = true;
      }
      assert(exception);
    }
    {
      bool exception = false;
      try
      {
        vector[2] = 0;
      }
      catch (std::exception ex)
      {
        exception = true;
      }
      assert(exception);
    }
  }


  // 'insert' implementation
  // Insert at the beginning 
  {
    VectorInt vector;
    vector.push_back(1);
    vector.push_back(2);

    int values[] = { 10, 11 };

    vector.insert(vector.begin(), std::begin(values), std::end(values));

    assert(vector[0] == 10);
    assert(vector[1] == 11);
    assert(vector[2] == 1);
    assert(vector[3] == 2);
  }

  // Insert at the end 
  {
    VectorInt vector;
    vector.push_back(1);
    vector.push_back(2);

    int values[] = { 10, 11 };

    vector.insert(vector.end(), std::begin(values), std::end(values));

    assert(vector[0] == 1);
    assert(vector[1] == 2);
    assert(vector[2] == 10);
    assert(vector[3] == 11);
  }

  // Insert in the middle
  {
    VectorInt vector;
    vector.push_back(1);
    vector.push_back(2);

    int values[] = { 10, 11 };

    vector.insert(vector.begin() + 1, std::begin(values), std::end(values));

    assert(vector[0] == 1);
    assert(vector[1] == 10);
    assert(vector[2] == 11);
    assert(vector[3] == 2);
  }

  // Insert empty range into vector does nothing
  {
    VectorInt vector;

    int values[] = { 10, 11 };

    vector.insert(vector.begin(), std::begin(values), std::begin(values));

    assert(vector.capacity() == 0);
    assert(vector.size() == 0);
  }

}