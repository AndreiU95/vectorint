#include <exception>
#include "VectorInt.h"


// Constructor: Here we should initialize our vector (in other words, the data memebers _begin, _end and _capacity) 
//with the default values
VectorInt::VectorInt()
{

}

const int* VectorInt::begin() const
{
  return _begin;
}

const int* VectorInt::end() const
{
  return _end;
}

//Capacity: This method returns the number of elements the vector can currently hold without allocating new memory,
//making room for more elements. It's different than the size of the vector. For example the vector can have enough
//space for 4 elements but none constructed in memory, therefore its size is zero. You may ask is this necessary?
//The answer is yes. A reason is performance. If capacity was always equal to the size then we would have to allocate
//memory each time we add an element to the vector.
const int VectorInt::capacity() const
{
  return _capacity;
}

// Size: Returns the number of elements the vector is currently holding. This should always be less than or equal
// to capacity.
const int VectorInt::size() const
{
  throw std::exception("Method not yet implemented.");
}

// Subscript operator: This allows the vector to be indexed as a typical C - style array, e.g.vector[2] to get the
// third element. Operators are just functions that can be called with a different syntax. We could have named it
// 'elementAtIndex' in which case we call it as vector.elementAtIndex(2) instead of vector[2].
// When writing functions or methods it's a good idea to think about valid and invalid arguments. In this case you
// should generate an std::exception if the index is out of bounds.
int& VectorInt::operator[](int index) const
{
  throw std::exception("Method not yet implemented.");
}

// Push back: When we create an vector object, begin_, end_ and capacity_ will
// be all equal to nullptr. So from the start we have no capacity and we need to allocate memory. We also need to think
// about the general case. We can push back an element no matter the vector size. Food for thought:
// - how do we check if there is enough space for an element
// - if there is no more room available we need to allocate a new block of memory and copy our elements over there
void VectorInt::push_back(int element)
{
  throw std::exception("Method not yet implemented.");
}

// Pop back: Popping elements is a lot easier than pushing them. We don't need to allocate new memory. But there is
// a case when a pop operation is illegal, in this case throw an std::exception.
void VectorInt::pop_back()
{
  throw std::exception("Method not yet implemented.");
}

// Insert: Wouldn't it be lovely if we could copy a C-style array into our vector? First and last point to a range in
// an array. In C++ the convention is to exclude the last element. In other words, we copy the interval [first, last).
// We will copy this range starting at position in our vector. Be careful, position should be a valid address in our
// vector memory block, and first and last should come from the same array.
void VectorInt::insert(const int* position, const int* first, const int* last)
{
  throw std::exception("Method not yet implemented.");
}

// Destructor: It's time to do some clean up. The destructor is called when the object is no longer needed and it's
// disposed of. This ensures that any memory we allocate will be properly deallocated.
VectorInt::~VectorInt()
{

}